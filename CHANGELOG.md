# Changelog of little-earley

Latest version: https://gitlab.com/lysxia/little-earley/-/blob/main/CHANGELOG.md

## 0.2.0.0

- Simplify signature of `ambiguities`; refactor accounting of locations in parse trees.
- Add informative error messages in `Result`

## 0.1.0.0

- First version. Released on an unsuspecting world.
