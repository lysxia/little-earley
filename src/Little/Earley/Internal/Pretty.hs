{-# LANGUAGE DefaultSignatures, FlexibleInstances #-}

module Little.Earley.Internal.Pretty where


-- | A class for ad hoc pretty-printers.
class PrettyPrint a where
  prettyPrint :: a -> String

  default prettyPrint :: Show a => a -> String
  prettyPrint = show

-- | Display the character without quotes.
instance PrettyPrint Char where
  prettyPrint = (: [])

-- | Display the string without quotes.
instance PrettyPrint String where
  prettyPrint = id


-- | Wrapper whose 'Show' instance uses 'PrettyPrint'.
--
-- This provides a convenient and explicit way to display results nicely in the
-- REPL. See also 'Little.Earley.pparse'.
newtype Pretty a = Pretty a

instance PrettyPrint a => Show (Pretty a) where
  show (Pretty a) = prettyPrint a
