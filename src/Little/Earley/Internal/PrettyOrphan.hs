{-# LANGUAGE FlexibleInstances, UndecidableInstances #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Little.Earley.Internal.PrettyOrphan where

import Little.Earley.Internal.Pretty

instance {-# OVERLAPPABLE #-} Show a => PrettyPrint a where
  prettyPrint = show
